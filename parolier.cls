%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% Parolier — by ChENSon Douce %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{parolier}

\LoadClass[7pt]{article}

\RequirePackage{verse}
\RequirePackage{multicol}
\RequirePackage{titlesec}
\RequirePackage{fontspec}
\RequirePackage{pdflscape}
\RequirePackage{hyperref}
\RequirePackage{xcolor}
\RequirePackage{tocloft}
\RequirePackage{fancybox}
\RequirePackage{emoji}

%%%% Hyperref

\definecolor{link_blue}{RGB}{0,0,97}
\hypersetup{
    colorlinks=true,       % false: boxed links; true: colored links
    linkcolor=link_blue,   % color of internal links (change box color with linkbordercolor)
    urlcolor=link_blue     % color of external links
}

%%%% Table of contents

\setlength\cftparskip{0pt}
\setlength\cftbeforesecskip{4pt}
%\setlength\cftaftertoctitleskip{2pt}
\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}

%%%% Polices

\newfontfamily\sectionfont[Path=fonts/,
                           BoldFont=URWBookman-DemiItalic.otf]
                          {URWBookman-DemiItalic.otf}

% Commandes redéfinissables
\newcommand{\internalSetupchanson}{}
\newcommand{\internalCleanupchanson}{}

\newenvironment{internalImagedroite}{}{}

\newcommand{\internalMusicLink}{}
\newcommand{\internalTonality}{}
\newcommand{\internalBeforeTitle}[1]{\makebox[\linewidth]{%
	\rlap{\smash{\internalMusicLink}}%
	\hfill%
	\begin{minipage}[c]{0.8\linewidth}\centering#1\end{minipage}
	\hfill%
	\llap{\smash{\internalTonality}}%
}}

%%%% Modifications générales

\titleformat{\section}{\LARGE\bfseries\sectionfont}{}{}{\internalBeforeTitle}
\titlespacing*{\section}{0pt}{*0}{*5}
\setlength{\vleftskip}{2mm} % Pour l'espace après \indicationligne

%%%% Page de garde

\renewcommand*{\maketitle}{%
    \begin{titlepage}
        \centering\hspace{0pt}

        \vfill{}

        {\LARGE \@title \par}
        \vspace{2em}
        {\Large\itshape\@author \par}

        \vfill{}\hspace{0pt}\vfill{}
        % ^^^ Dirty way to have fractional vertical alignment: title is placed
        % at 1/3 of the page
    \end{titlepage}
}

%%%% Inclusion de pages de chansons

\newlength{\saveleftmargini}
\newenvironment{chanson}{
    % Change *this* verse left margin
    \setlength{\saveleftmargini}{\leftmargini}
    \setlength{\leftmargini}{1em}

    \internalSetupchanson{}
    \begin{internalImagedroite}
    \begin{verse}
}{
    \end{verse}
    \end{internalImagedroite}
    \internalCleanupchanson{}

    \setlength{\leftmargini}{\saveleftmargini}
}

\newcommand{\internalNouvellechanson}{
    \renewenvironment{internalImagedroite}{}{}
    \renewcommand{\internalMusicLink}{}
    \renewcommand{\internalTonality}{}
}

\newcommand{\inputchanson}[1]{\chansonunecol{#1}} %< Alias, pour les flemmard·e·s

\newcommand{\chansonunecol}[1]{ %< Sur une colonne
    \internalNouvellechanson{}
    \input{chansons/#1}
    \pagebreak
}

\newcommand{\chansondeuxcol}[1]{ %< Sur deux colonnes
    \internalNouvellechanson{}
    \renewcommand{\internalSetupchanson}{
        \begin{multicols}{2}
    }
    \renewcommand{\internalCleanupchanson}{
        \end{multicols}
    }

    \input{chansons/#1}
    \pagebreak

    \renewcommand{\internalSetupchanson}{}
    \renewcommand{\internalCleanupchanson}{}
}


%%%% Formattage des chansons

\setcounter{secnumdepth}{0}

\newcommand\titre[2][\relax]{%
 	\ifx\relax#1%
		\section{#2}%
	\else
		\section[#1]{#2}%
	\fi
}

\newcommand{\lienmusique}[1]{ %< À mettre AVANT \titre{}
    \renewcommand{\internalMusicLink}{\href{#1}{\emoji{play-button}}}
}

\newcommand{\tonalite}[1]{ %< À mettre AVANT \titre{}
	\renewcommand{\internalTonality}{\raisebox{\fboxsep}{\Ovalbox{\texttt{\textit{\textmd{\normalsize #1}}}}}}
}

\newcommand{\structure}[1]{
    \vspace{0.6em}
    \textbf{[#1]}
    \vspace{0.6em}
}

\newcommand{\structuredef}[1]{
    \hspace{-0.5em}\textbf{#1} \\*
}

\newcommand{\indication}[1]{
    \hspace{1em}--~\textit{#1}~--
}

\newcommand{\voixoff}[1]{
    {(\small\textit{#1})}
}

\newcommand{\indicationligne}[1]{
    \flagverse{\textbf{[\texttt{#1}]}}
}

%%%% Fluff

\newcommand{\imagechanson}[1]{
    \vspace{1em}
    \begin{center}
        \includegraphics[
            height=\dimexpr\textheight-\pagetotal-2em\relax,
            width=0.9\textwidth,
            keepaspectratio]{imgs/#1}
    \end{center}
}

\newcommand{\imagedroite}[2]{  %< À mettre AVANT \begin{chanson}
    \renewenvironment{internalImagedroite}{
        \begin{minipage}[c]{\dimexpr\textwidth-#2-2em}
    }
    {
        \end{minipage}\hfill\begin{minipage}{#2}
            {
                \centering
                \includegraphics[width=\textwidth]{imgs/#1}
            }
        \end{minipage}
        \vfill
    }
}
